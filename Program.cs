﻿using System;
using System.Collections.Generic;
using System.Data;

//To run the program properly, you MUST run the program by typing in 'dotnet run' into the terminal!
//Otherwise, the input function will not work!

//https://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour


namespace c_sharp_program
{
    class Program
    {
        public static List<int> sequence = new List<int>();
        public static int reps;

        static void Main(string[] args)
        {
            System.Console.WriteLine("\nFibonacci Sequence!");
            System.Console.Write("Enter the desired depth of the Fibonacci Sequence: ");
            int.TryParse(System.Console.ReadLine(), out int val);
            Program.reps = val;
            Console.WriteLine("------------------");
            Fibonacci();
            Console.WriteLine();

            List<int> sumNumbers = new List<int>() {1, 2, 3, 4, 6, 10, -5};
            Console.WriteLine("Loop Sums!");
            Console.WriteLine("----------");
            Console.WriteLine("For Sum: " + ForAdd(sumNumbers));
            Console.WriteLine("While Sum: " + WhileAdd(sumNumbers));
            Console.WriteLine("Recursive Sum: " + RecursiveAdd(sumNumbers, sumNumbers.Count - 1));
            Console.WriteLine();
            List<string> nines = new List<string>() {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

            PainInTheButt(nines);
        }

        static void Fibonacci()
        {
            Recursive(Program.reps - 1);

            for (int i = 0; i < Program.sequence.Count; i++){
                Console.Write(Program.sequence[i] + " ");
            }
            Console.WriteLine();

        }

        static void Recursive(int loop)
        {
            if (loop <= 1)
            {
                sequence.Add(1);
                sequence.Add(1);
            }
            else
            {
                Recursive(loop - 1);
                int number = sequence[loop - 1] + sequence[loop - 2];
                sequence.Add(number);
            }

        }

        static int ForAdd(List<int> numbers){
            int sum = 0;
            for (int i = 0; i < numbers.Count; i++){
                sum += numbers[i];
            }
            return sum;
        }
        static int WhileAdd(List<int> numbers){
            int sum = 0, i = 0;
            while (i < numbers.Count){
                sum += numbers[i];
                i++;
            }
            return sum;
        }
        static int RecursiveAdd(List<int> numbers, int counter){
            if (counter == 0){
                return numbers[counter];
            }
            else{
                return numbers[counter] + RecursiveAdd(numbers, counter - 1);
            }
        }

        static void PainInTheButt(List<string> nines){
            int var = 100;
            string goal = var.ToString();
            string str;
            List<string> formulae = new List<string>();
            for (int i = 0; i < Math.Pow(8, nines.Count - 1); i++){
                string formula = "";
                string operations = "";
                str = Convert.ToString(i, 8);
                while (str.Length < 8){
                    str = "0" + str;
                }
                for (int j = 0; j < str.Length; j++){
                    if (str[j] == '0'){
                        operations = operations + "-";
                    }
                    else if (str[j] == '1'){
                        operations = operations + "+";
                    }
                    else{
                        operations = operations + "_";
                    }
                }
                for (int j = 0; j < str.Length; j++){
                    formula = formula + nines[j] + operations[j];
                }
                formula = formula + nines[8];
                formulae.Add(formula);
                formula = "";

                if (i % 1000000 == 0 && i > 0){
                    Console.WriteLine((i / 1000000) + ".0 M Lines Compiled...");
                }
            }

            for (int i = 0; i < formulae.Count; i++){
                formulae[i]=formulae[i].Replace("_", "");
            }
            
            List<string> correct = new List<string>();


            DataTable dt = new DataTable();
            
            for (int i = 0; i < formulae.Count; i++){
                if (i % 1000000 == 0 && i > 0){
                    Console.WriteLine((i / 1000000) + ".0 M Lines Computed...");
                }
                var num = dt.Compute(formulae[i], "");
                if (num.ToString() == goal){
                    correct.Add(formulae[i]);
                }
            }

            Console.WriteLine("Cleaning up...");

            for (int i = 0; i < correct.Count; i++){
                correct[i] = correct[i].Replace(" ", String.Empty);
                correct[i] = correct[i].ToString();
            }

            List<string> singular = new List<string>();

            for (int i = 0; i < correct.Count; i++){
                if (singular.FindIndex(x => correct[i] == x) >= 0){}
                else{
                    singular.Add(correct[i]);
                }
            }

            Console.WriteLine("\nFormulae with sum of 100: " + singular.Count);
            Console.WriteLine("----------------------------");
            for (int i = 0; i < singular.Count; i++){
                Console.WriteLine(singular[i]);
            }
            Console.WriteLine();
        }



    }


}
